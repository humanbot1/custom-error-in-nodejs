const { User, mongoConnection } = require("models/business");

const {
  DomainError,
  ResourceNotFoundError,
  Forbidden,
  BadRequest,
  Unauthorized,
  Conflict,
  InternalServerError
} = require("./http_error");


const { describe, it } = require('mocha');
const { expect } = require('chai');

describe('When searching a user', () => {
    it('should throw an error if the person cannot be found', async () => {
        expect(() =>  await findUser('Manoj Karmocha')).to.throw(ResourceNotFoundError);
    });
});