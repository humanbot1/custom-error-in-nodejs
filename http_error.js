
class DomainError extends Error {
  constructor(message) {
    super(message);
    // This clips the constructor invocation from the stack trace.
   Error.captureStackTrace(this, DomainError);

   // Ensure the name of this error is the same as the class name
    this.name = 'DomainError';
  }
}
class ResourceNotFoundError extends DomainError {
  constructor(resource, query) {
    super(`Resource ${resource} was not found.`);
    this.data = { resource, query };
  }
}

class InternalServerError extends DomainError {
  constructor(error) {
    // const extraMessage = message || 'InternalServerError';
    super(error.message);
    this.data = {error};
  }
}

class Forbidden extends Error {
  constructor(message) {
    const extraMessage = message || 'Forbidden';
    super(`403|${extraMessage}`);

    // Maintains proper stack trace for where our error was thrown (only available on V8)
      Error.captureStackTrace(this, Forbidden);
    this.name = 'Forbidden';
  }
}

class BadRequest extends Error {
  constructor(message) {
    const extraMessage = message || 'BadRequest';
    super(`400|${extraMessage}`);

    // Maintains proper stack trace for where our error was thrown (only available on V8)
      Error.captureStackTrace(this, BadRequest);
    this.name = 'BadRequest';
  }
}

class Unauthorized extends Error {
  constructor(message) {
    const extraMessage = message || 'Unauthorized';
    super(`401|${extraMessage}`);

    // Maintains proper stack trace for where our error was thrown (only available on V8)
      Error.captureStackTrace(this, Unauthorized);
    this.name = 'Unauthorized';
  }
}

class Conflict extends Error {
  constructor(message) {
    const extraMessage = message || 'Conflict';
    super(`409|${extraMessage}`);

    // Maintains proper stack trace for where our error was thrown (only available on V8)
      Error.captureStackTrace(this, Conflict);
    this.name = 'Conflict';
  }
}



module.exports = {
  DomainError,
  ResourceNotFoundError,
  Forbidden,
  BadRequest,
  Unauthorized,
  Conflict,
  InternalServerError,
};

