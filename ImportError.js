// Get the mongo connection, business and subscription model
const { User } = require("models/user");
const mongoose = require('mongoose');

const {
  DomainError,
  ResourceNotFoundError,
  Forbidden,
  BadRequest,
  Unauthorized,
  Conflict,
  InternalServerError
} = require("./http_error");

// Create handler as lambda
const handlerUser = async (event, context) => {
  // We are keeping an open DB connection between invokes. This means that the node event loop will
  // never empty. We need to tell Lambda not to wait for the event loop to empty before exiting.
  // This is required for every lambda that interacts with the DB
  // eslint-disable-next-line no-param-reassign
  context.callbackWaitsForEmptyEventLoop = false;

// Need to create a mongoose connection to create model and also check user in model
// connect mongoose /////////////// TO DO /////////////////////////////

  // retrieve the person from the path parameters from frontend
  const { user } = event;
  const query = { user };

  let person;

  try {
    person = await Person.findOne(query);
  } catch (error) {
    throw new InternalServerError(error);
  }

  // const query = {person}
  if (!person) {
    // This is the logic to show custom error using our http error stacktrace to find out the right error
    throw new ResourceNotFoundError("person", query);
  }
  return person;
};

// Export the module as handler
module.exports = { handlerUser };
